/********************************************************************************
** Form generated from reading UI file 'vyhladavanie.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VYHLADAVANIE_H
#define UI_VYHLADAVANIE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_VyhladavanieClass
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QLineEdit *lineEdit;
    QPushButton *pushButton;

    void setupUi(QDialog *VyhladavanieClass)
    {
        if (VyhladavanieClass->objectName().isEmpty())
            VyhladavanieClass->setObjectName(QStringLiteral("VyhladavanieClass"));
        VyhladavanieClass->resize(339, 400);
        gridLayout = new QGridLayout(VyhladavanieClass);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        lineEdit = new QLineEdit(VyhladavanieClass);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        verticalLayout->addWidget(lineEdit);

        pushButton = new QPushButton(VyhladavanieClass);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        verticalLayout->addWidget(pushButton);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);


        retranslateUi(VyhladavanieClass);
        QObject::connect(pushButton, SIGNAL(clicked()), VyhladavanieClass, SLOT(hladaj()));

        QMetaObject::connectSlotsByName(VyhladavanieClass);
    } // setupUi

    void retranslateUi(QDialog *VyhladavanieClass)
    {
        VyhladavanieClass->setWindowTitle(QApplication::translate("VyhladavanieClass", "Vyhladavanie", 0));
        pushButton->setText(QApplication::translate("VyhladavanieClass", "Hladaj", 0));
    } // retranslateUi

};

namespace Ui {
    class VyhladavanieClass: public Ui_VyhladavanieClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VYHLADAVANIE_H
