#include "vyhladavanie.h"

Vyhladavanie::Vyhladavanie(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	ui.verticalLayout->setAlignment(Qt::AlignTop);
}

Vyhladavanie::~Vyhladavanie()
{

}

void Vyhladavanie::hladaj()
{
	QString zaklad = "http://www.google.sk/search?q=";
	vyhladaj = ui.lineEdit->text();
	url.setUrl(QString(zaklad + vyhladaj));
	reply = qnam.get(QNetworkRequest(url));
	connect(reply, &QNetworkReply::finished, this, &Vyhladavanie::vysledky);
}

void Vyhladavanie::vysledky()
{
	if (!label_linky.isEmpty())
	{
		for each (QLabel *hyperlink in label_linky)
		{
			ui.verticalLayout->removeWidget(hyperlink);
			delete hyperlink;
		}		
	}
	QString vysledok = reply->readAll();
	QStringList komplet = vysledok.split("<a href=\"/url?q="); //rozdelenie stringov podla retazca
	QStringList patka = komplet.mid(1, 5);

	for each (QString linkaAmeno in patka)
	{
		QString link = linkaAmeno.split("&amp;").first();
		QString title = "";
		if (linkaAmeno.contains("title=\"")) //obrazok
		{
			title = linkaAmeno.split("title=\"").at(1);
			title = title.remove(title.indexOf("\""), title.length());
		}
		else //obycajny odkaz
		{ 
			title = linkaAmeno.split("\">").at(1);
			title = title.remove(title.indexOf("</a>"), title.length());
		}
		QLabel *hyperLink = new QLabel("<a href = \"" + link + "\"> " + title + " - " + link + " </a>");
		hyperLink->setOpenExternalLinks(true);
		hyperLink->setTextInteractionFlags(Qt::TextBrowserInteraction);
		ui.verticalLayout->addWidget(hyperLink);
		label_linky.append(hyperLink);
	}


	/*int index = komplet.indexOf("&", 0); //malo by najst index &
	komplet = komplet.mid(5, index); //a od indexu odseknut pocet znakov*/

	/*link = link.replaceInStrings(QString("</a>"), QString(" "));
	link = link.replaceInStrings(QString("<b>"), QString(" "));
	link = link.replaceInStrings(QString("</b>"), QString(" "));*/

	//ui.listWidget->addItems(test); //vypis do list widgetu
}
