#ifndef VYHLADAVANIE_H
#define VYHLADAVANIE_H

#include <QtWidgets/QDialog>
#include "ui_vyhladavanie.h"
#include <QNetworkAccessManager>
#include <QUrl>
#include <QtNetwork>
#include <QString>
#include <QFile>
#include <QTextEdit>
#include <qlabel.h>

class Vyhladavanie : public QDialog
{	
	Q_OBJECT

public:
	Vyhladavanie(QWidget *parent = 0);
	~Vyhladavanie();

public slots:
	void hladaj();
	void vysledky();

private:
	Ui::VyhladavanieClass ui;
	QUrl url;
	QNetworkAccessManager qnam;
	QNetworkReply *reply;
	QString vyhladaj;
	QList<QLabel*> label_linky;
};

#endif // VYHLADAVANIE_H
